package com.bta;

import java.util.Scanner;

import static jdk.nashorn.internal.objects.NativeMath.round;


public class Mokinys extends Zmogus {

    static int mokiniuKiekis = 0;
    static int[][] klases = new int [12][2];

    private int klase;
    private int[] pazymiai = new int[20];
    private int pazymiuKiekis = 0;
    private Scanner reader = new Scanner(System.in);  // Reading from System.in
    private double vidurkis;


    static void rikiuotiMokinius(Mokinys[] mokiniai) {
        for(int i = 0; i < Mokinys.mokiniuKiekis - 1; i++) {
            for(int j = 0; j < Mokinys.mokiniuKiekis -1; j++) {
                if(mokiniai[j].getVidurkis() < mokiniai[j+1].getVidurkis()) {
                    Mokinys temp  = mokiniai[j];
                    mokiniai[j] = mokiniai[j+1];
                    mokiniai[j+1] = temp;
                }
            }
        }

        Mokinys.spausdintiMokinius(mokiniai);
    }

    static void spausdintiMokinius(Mokinys[] mokiniai) {
        for(int i = 0; i < Mokinys.mokiniuKiekis; i++) {
            System.out.println(mokiniai[i].getVardasPavarde() + " " + mokiniai[i].getVidurkis());
        }
    }

    static void klasesInformacija(Mokinys[] mokiniai, int klase) {
        double klasesVidurkiuSuma = 0.0;
        int klasesMokiniuKiekis = 0;
        double klasesPazymiuVidurkis = 0.0;
        System.out.println("Mokiniu kurie mokosi klaseje: " + klase + " sarasas");
        for(int i = 0; i < Mokinys.mokiniuKiekis; i++) {
            if(mokiniai[i].getKlase() == klase) {
                System.out.println(mokiniai[i].getVardasPavarde());
                klasesVidurkiuSuma += mokiniai[i].getVidurkis();
                klasesMokiniuKiekis++;
            }
        }

        klasesPazymiuVidurkis = klasesVidurkiuSuma / klasesMokiniuKiekis;
        System.out.println("Klaseje is viso yra: " + klasesMokiniuKiekis + " mokiniu");
        System.out.println("Klases mokiniu vidurkis");
        System.out.println(klasesPazymiuVidurkis);
    }



    // Mokinio klases konstruktorius
    Mokinys(String vardas, String pavarde) {
        // Kreipiames i paveldimos klases konstruktoriu
        super(vardas, pavarde);
        System.out.println("Iveskite " + this.getVardasPavarde() + " klase: ");
        this.klase = reader.nextInt();
        System.out.println(this.getVardasPavarde() + " priskirtas klasei: " + this.klase);

        this.priskirtiPazymius();
        Mokinys.mokiniuKiekis++;
    }

    // Mokinio klases konstruktorius kai paduodama tik mokinio klase
    Mokinys(String vardas, String pavarde, int klase) {
        // Kreipiames i paveldimos klases konstruktoriu
        super(vardas, pavarde);
        this.klase = klase;
        this.priskirtiPazymius();
        Mokinys.mokiniuKiekis++;

    }

    // Mokinio klases konstruktorius kai paduodama ir klase ir pazymiai
    Mokinys(String vardas, String pavarde, int klase, int[] pazymiai) {
        // Kreipiames i paveldimos klases konstruktoriu
        super(vardas, pavarde);
        this.klase = klase;
        this.pazymiai = pazymiai;
        this.pazymiuKiekis = pazymiai.length;
        Mokinys.mokiniuKiekis++;
    }

    public void priskirtiPazymius() {

        // Prasome ivesti pazymius
        System.out.println("Iveskite " + this.getVardasPavarde() + " pazymius: ");
        int vartotojoIvestasPazymys = reader.nextInt();
        this.addPazymys(vartotojoIvestasPazymys);

        while (vartotojoIvestasPazymys > 0 && vartotojoIvestasPazymys <= 10) {
            System.out.println("Iveskite " + this.getVardasPavarde() + " pazymius: ");
            vartotojoIvestasPazymys = reader.nextInt();
            if (vartotojoIvestasPazymys > 0 && vartotojoIvestasPazymys <= 10) {
                this.addPazymys(vartotojoIvestasPazymys);
            }
        }
    }

    @Override
    public String getVardasPavarde() {
        if(this.klase == 12) {
            return "Abiturientas"  + this.getVardas() +  " " + this.getPavarde() + " " + this.klase;

        } else {
            return this.getVardas() +  " " + this.getPavarde() + " " + this.klase;
        }
    }

    public void addPazymys(int pazymys) {
        this.pazymiai[this.pazymiuKiekis] = pazymys;
        this.pazymiuKiekis++;
        this.skaiciuotiVidurki();
    }

    public void skaiciuotiVidurki() {
        int suma = 0;
        for (int i = 0; i < this.pazymiuKiekis; i++) {
            suma += this.pazymiai[i];
        }

        this.vidurkis = 1.0 * suma / this.pazymiuKiekis;
        this.vidurkis = round(2, this.vidurkis);
    }

    public int getPazymiuSuma() {
        int suma = 0;
        for(int i = 0; i < this.pazymiuKiekis; i++) {
            suma += this.pazymiai[i];
        }

        return suma;
    }

    public int getKlase() {
        return this.klase;
    }

    public void setKlase(int klase) {
        this.klase = klase;
    }

    public int[] getPazymiai() {
        return pazymiai;
    }

    public void setPazymiai(int[] pazymiai) {
        this.pazymiai = pazymiai;
    }

    public double getVidurkis() {
        return vidurkis;
    }
}