package com.bta;

import java.util.Scanner;

public class Main {
    static private Scanner reader = new Scanner(System.in);  // Reading from System.in

    public static void main(String[] args) {
	// write your code here


        Mokinys[] mokiniai = new Mokinys[10];

        for(int i = 0; i < 10; i++) {
            System.out.println("Iveskite mokinio varda:");
            String vartotojoIvestasVardas = reader.next();

            if(vartotojoIvestasVardas.equals("baigiau")) {
                break;
            }

            System.out.println("Iveskite mokinio pavarde:");
            String vartotojoIvestasPavarde = reader.next();

            mokiniai[Mokinys.mokiniuKiekis] = new Mokinys(vartotojoIvestasVardas, vartotojoIvestasPavarde);
            System.out.println("Ivestas naujas mokinys");
        }



        // Kvieciu statine funkcija rikiuoti mokinius
        Mokinys.rikiuotiMokinius(mokiniai);

        Mokinys.klasesInformacija(mokiniai, 5);


    }
}
