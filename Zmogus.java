package com.bta;

public class Zmogus {
    private String vardas;
    private String pavarde;


    // Konstruktorius
    Zmogus(String vardas, String pavarde) {
        this.vardas = vardas;
        this.pavarde = pavarde;
    }

    public String getVardas() {
        return this.vardas;
    }


    public String getPavarde() {
        return this.pavarde;
    }

    public String getVardasPavarde() {
        return this.vardas + " " + this.pavarde;
    }
}
